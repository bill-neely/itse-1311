// Execute on load
$(document).ready(function() {
  showQueryString();
});

function showQueryString() {
  queryString = getUrlVars();
  result = "<p>No query string found</p>";
  if (queryString.length != 0) {
    result = "";
    queryString.forEach( function(item) {
      result += "<p>" + item + ' = ' + (queryString[item]) + "</p>";
    });
  }
  $('#query-string-inner').html(result);
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hasQueryString = ((window.location.href.indexOf('?')) != -1) ;
    if (hasQueryString) {
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&') ;
      for(var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = urldecode(hash[1]);
      }
    }
    return vars;
}

function urldecode(str) {
   return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}
