$(document).ready(function() {
    $('#load-pokemon').click(loadThePokemon);
    $('#load-left').click(loadLeft);
    $('#load-right').click(loadRight);
    $(document).keypress(function(e) {
      if(e.which == 13) {
        loadThePokemon();
      }
    });
});

function clearAllBoxes() {
  smallLoad = '<img class="small-loading" src="http://www.lettersmarket.com/uploads/lettersmarket/blog/loaders/common_metal/ajax_loader_metal_512.gif">';
  $('#div-description label').html(smallLoad);
  $('#list-genders').html(smallLoad);
  $('#list-weaknesses').html(smallLoad);
}

function loadThePokemon() {
  clearAllBoxes();
  $('#div-loading').show();
  $('#div-content').hide();
  $('#div-error').hide();
  var pokemonID = $('#pokemon-number').val().toLowerCase().trim();
  $.ajax({
    url: 'https://pokeapi.co/api/v2/pokemon/' + pokemonID + '/',
    success: apiPokemonSuccess,
    error: apiPokemonFailed
  });

  function apiPokemonSuccess(pokemonData) {
    val = $('#pokemon-number').val(pokemonData.id);
    loadSpeciesData(pokemonData);
    loadWeaknesses(pokemonData);
    loadTitle(pokemonData);
    loadSprites(pokemonData);
    loadCharacteristics(pokemonData);
    loadTypes(pokemonData);
    loadStats(pokemonData);
    $('#div-content').show();
    $('#div-loading').hide();
  }

  function apiPokemonFailed() {
    $('#div-error').show();
    $('#div-loading').hide();
  }

}

function loadLeft() {
  val = $('#pokemon-number').val();
  val--;
  $('#pokemon-number').val(val);
  loadThePokemon();
}

function loadRight() {
  val = $('#pokemon-number').val()
  val++;
  $('#pokemon-number').val(val);
  loadThePokemon();
}

function loadCharacteristics(pokemonData) {
  $('#char-height').html(pokemonData.height);
  $('#char-weight').html(pokemonData.weight);
  loadCharacteristicsAbility(pokemonData);
}

function loadCharacteristicsAbility(pokemonData) {
  $('#list-abilities').html('');
  perLine = 1;
  counter = 0;
  pokemonData.abilities.forEach( function(entry) {
    $('#list-abilities').append("<label class='ability ability-" + entry.ability.name + "'>" + entry.ability.name + "</label>");
    counter++;
    if (counter % perLine == 0) {
      $('#list-abilities').append("<br>");
    }
  });
};

function loadDescription(speciesData) {
  description = "Couldn't find a description";
  speciesData.flavor_text_entries.some( function( entry ) {
    if (entry.language.name === 'en') {
      description = entry.flavor_text;
      return true;
    }
  });
  $('#div-description label').html(description);
}

function loadGenders(speciesData) {
  $('#list-genders').html('');
  if (speciesData.gender_rate === -1) {
    $('#list-genders').append("<label class='gender genderless'>Genderless</label>  ");
  }
  else if (speciesData.gender_rate === 0) {
    $('#list-genders').append("<label class='gender gender-male'>Male</label>");
  }
  else {
    $('#list-genders').append("<label class='gender gender-male'>Male</label><br>");
    $('#list-genders').append("<label class='gender gender-female'>Female</label>");
  }
}

function loadSpeciesData(pokemonData) {
  $.ajax({
    url: pokemonData.species.url,
    success: apiSpeciesSuccess,
    error: apiSpeciesFailed
  });

  function apiSpeciesSuccess(speciesData) {
    loadDescription(speciesData);
    loadGenders(speciesData);
  }

  function apiSpeciesFailed() {
    $('#div-description').html("Failed to load the Species");
  }
}

function loadSprites(pokemonData) {
  var missingImage = 'http://www.clipartbest.com/cliparts/jTx/6Mj/jTx6MjGBc.gif';
  if (pokemonData.sprites.front_default != null) {
    $('#front-sprite').attr("src", pokemonData.sprites.front_default)
  }
  else {
    $('#front-sprite').attr("src", missingImage)
  }
  if (pokemonData.sprites.back_default != null) {
    $('#back-sprite').attr("src", pokemonData.sprites.back_default)
  }
  else {
    $('#back-sprite').attr("src", missingImage)
  }
}

function loadStats(pokemonData) {
    pokemonData.stats.forEach(function(entry) {
      $('#div-stats td.stat-' + entry.stat.name).html(entry.base_stat);
    });
}

function loadTitle(pokemonData) {
  name = pokemonData.name;
  id = pokemonData.id;
  nameAndID = '#' + id + ' - ' + name;
  $('#title-text').html(nameAndID);
}

function loadTypes(pokemonData) {
  $('#list-types').html('');
  pokemonData.types.forEach( function(entry) {
    $('#list-types').append("<label class='type " + entry.type.name + "'>" + entry.type.name + "</label>");
  });
}

function loadWeaknesses(pokemonData) {
  $.ajax({
      url: pokemonData.types[0].type.url,
      success: apiTypeSuccess,
      error: apiTypeFailed
  });

  function apiTypeSuccess(typeData) {
    var listOfWeakness = [];
    typeData.damage_relations.double_damage_from.forEach( function(entry) {
      if (!listOfWeakness.includes(entry.name)) {
        listOfWeakness.push(entry.name);
      }
    });
    perLine = 4;
    counter = 0;
    $('#list-weaknesses').html('');
    listOfWeakness.forEach(function(entry) {
      $('#list-weaknesses').append("<label class='weakness " + entry + "'>" + entry + "</label>");
      counter++;
      if (counter % perLine === 0) {
        $('#list-weaknesses').append("<br>");
      }
    });
  }

  function apiTypeFailed() {
    $('#list-weaknesses').html("Failed to load the Type");
  }
}
