$(document).ready(function() {
  $('#btnRoll').click(rollTheDice);
});

var rollIntervalms = 200

function rollTheDice() {
  $('#start').text(getTime());
  numberOfUpdates = 0;
  rolling = setInterval(updateFaces, rollIntervalms)
}




function updateFaces() {
  updateFace($('#die1'));
  updateFace($('#die2'));
  updateFace($('#die3'));
  updateFace($('#die4'));
  updateFace($('#die5'));
  updateFace($('#die6'));
  numberOfUpdates++;
  $('#counter').text  (numberOfUpdates);
  if (numberOfUpdates >= 3000 / rollIntervalms) { // roll for 3 seconds
    clearInterval(rolling);
    $('#end').text(getTime());
  }
}
function getTime() {
  return (new Date()).getTime() % 100000000;
}

function updateFace(die) {
  face = Math.floor(Math.random() * 6) + 1;
  className = 'die-face die' + face.toString();
  $(die).attr('class', className);
}
