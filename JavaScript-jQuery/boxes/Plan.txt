
  1. create html containers with default content
      control-box with single text box, two checkboxes, and button
      content container (two sub containers)
          average, sum buttons
          array of text boxes to run math functions on
  2. setup .js to create the control-box
  3. setup .js to create the dynamic average and sum buttons
  4. setup .js to create the array of text-boxes for math functions
  5. Implement sum and avg functions
  6. validation to make sure only numbers are being entered.
  7. Setup style
