$(document).ready(function() {
  setupControlBox();
});

function setupControlBox() {
  setupHowMany();
  setupCheckBoxes();
  setupBuildButton();
}

function setupHowMany() {
  $('#how-many').html('');
  $('#how-many').append("<label for='numberToMake'>Number of Boxes:</label>");
  $('#how-many').append("<input id='numberToMake' type='text' class='invalidBox'>");
  $('#numberToMake').change(validateNumberToMake);
}

function validateNumberToMake() {
    if (isNaN($('#numberToMake').val())) {
      $('#buildButton').prop("disabled",true);
      $('#numberToMake').addClass('invalidBox');
    }
    else {
      $('#buildButton').prop("disabled",false);
      $('#numberToMake').removeClass('invalidBox');
    }
}

function setupCheckBoxes() {
  $('#which-buttons').html('');
  $('#which-buttons').append("<input id='averageCheckBox' type='checkbox'>");
  $('#which-buttons').append("<label>Average</label>");
  $('#which-buttons').append("<input id='sumCheckBox' type='checkbox'>");
  $('#which-buttons').append("<label>Sum</label>");
}

function setupBuildButton() {
  $('#build-button').html("<button id='buildButton' class='btn btn-info'>Build The Boxes</button>");
  $('#buildButton').click(buildContent);
  $('#buildButton').prop("disabled",true);
}


function buildContent() {
  buildMathButtons();
  buildDynamicBoxes();
}

function buildMathButtons() {
  $('#math-buttons').html('');
  if ($('#averageCheckBox').is(":checked")) {
    $('#math-buttons').append("<button id='avgButton' class='btn'>Average</button>");
    $('#avgButton').click(avgDynamicBoxes);
  }
  if ($('#sumCheckBox').is(":checked")) {
    $('#math-buttons').append("<button id='sumButton' class='btn'>Sum</button>");
    $('#sumButton').click(sumDynamicBoxes);
  }

}

function buildDynamicBoxes() {
  $('#dynamic-boxes').html('');
  for ( i = 1; i <= $('#numberToMake').val(); i++ ) {
    $('#dynamic-boxes').append("<label for='numberToMake'>Box " + i + ": </label>");
    $('#dynamic-boxes').append("<input type='text'><br>");
  }
  $('#dynamic-boxes input').change(validateDynamicBox);
}

function validateDynamicBox() {
    if (isNaN($(this).val())) {
      $(this).addClass('invalidBox');
    }
    else {
      $(this).removeClass('invalidBox');
    }
}

function avgDynamicBoxes() {
  total = 0;
  count = 0;
  $('#dynamic-boxes input').each(function() {
    total += parseInt($(this).val());
    count += 1;
  });
  average = total / count;
  $('#math-result').html("The average is: " + average);
}

function sumDynamicBoxes() {
  total = 0;
  $('#dynamic-boxes input').each(function() {
    total += parseInt($(this).val());
  });
  $('#math-result').html("The sum is: " + total);
}
