$(document).ready(function() {
    $('#get-pokemon').click(loadThePokemon);
});

function loadThePokemon() {
  var pokemonID = $('#pokemon-number').val();
  $.ajax({
    url: 'http://pokeapi.co/api/v2/pokemon/' + pokemonID + '/',
    success: apiSuccess,
    error: apiFailed
  });
}

function apiFailed() {
  $('#content-header').html("Something went wrong");
}

function apiSuccess(pokemonData) {
  loadTheTitle(pokemonData);
  loadPokemonImage(pokemonData);
  loadPokemonTypes(pokemonData);
  loadPokemonDescription(pokemonData);
  loadPokemonWeakness(pokemonData);
}

function loadTheTitle(pokemonData) {
  $('#content-header').html(pokemonData.name);
}

function loadPokemonImage(pokemonData) {
  var pokemonFrontImage = pokemonData.sprites.front_default;
  $('#pokemon-image').html("<img src='" + pokemonFrontImage + "'>")
}

function loadPokemonTypes(pokemonData) {
  $('#pokemon-type').html('');
  pokemonData.types.forEach(function( entry ) {
    $('#pokemon-type').append('type=' + entry.type.name + '<br>');
  });
}

function loadPokemonDescription(pokemonData) {
  $.ajax({
    url: pokemonData.species.url,
    success: apiDescriptionSuccess,
    error: apiDescriptionFailed
  });

  function apiDescriptionSuccess(speciesData) {
    speciesData.flavor_text_entries.some(function(entry) {
      if (entry.language.name == 'en') {
        $('#pokemon-description').html(entry.flavor_text);
        return true;
      }
    });
  }

  function apiDescriptionFailed() {
    alert("apiDescriptionFailed");
  }
}

function loadPokemonWeakness(pokemonData) {
  $.ajax({
    url: pokemonData.types[0].type.url,
    success: apiTypeSuccess,
    error: apiTypeFail
  });

  function apiTypeSuccess(typeData) {
    $('#pokemon-weakness').html('');
    typeData.damage_relations.double_damage_from.forEach(function (entry) {
      $('#pokemon-weakness').append('weakness=' + entry.name +'<br>');
    });
  }

  function apiTypeFail() {
    alert("apiTypeFail");
  }
}
