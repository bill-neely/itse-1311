$(document).ready(function() {
  loadDropDowns();
  $('.school-dropdown select').change(loadPanel);
});

function loadPanel(dropDownPicked) {
  target = $(this).attr('for');
  content = $('#' + $(this).val()).html();
  $('#' + target).html(content);
}

function loadDropDowns() {
  $('.school-dropdown select').append($('<option>', {
      value: 'blank-panel',
      text: 'Choose a School...'
  }));
  $('.school-dropdown select').append($('<option>', {
      value: 'Baylor-panel',
      text: 'Baylor University'
  }));
  $('.school-dropdown select').append($('<option>', {
      value: 'UT-panel',
      text: 'University of Texas'
  }));
  $('.school-dropdown select').append($('<option>', {
      value: 'TexTech-panel',
      text: 'Texas Tech University'
  }));
  $('.school-dropdown select').append($('<option>', {
      value: 'TexAM-panel',
      text: 'Texas A&M University'
  }));
}
